.. SPDX-License-Identifier: CC-BY-SA-4.0

*********
UEFI spec
*********

This chapter discusses specific UEFI implementation details.

UEFI Version
============
This document uses version 2.9 of the UEFI specification [UEFI]_.

UEFI Compliance
===============

All [UEFI]_ features required by [EBBR]_ are assumed.

UEFI extra dependencies
=======================

The capsules installed using the procedure defined in this document must be formatted according
to the FMP defined format [UEFI]_ § 23.1 - Firmware Management Protocol.
The FMP instance must provide the GetImageInfo and SetImage functions in order to be used as the
UpdateCapsule backend.


ESRT
----

[UEFI]_ defines the System Resource Table (ESRT) § 23.4 - EFI System Resource Table.

Each entry in the ESRT describes a device or system firmware resource that can be targeted
by a firmware capsule update. Each entry in the ESRT will also be used to report status of the
last attempted update.

The UEFI specification defines a mapping between the ESRT fields and the
EFI_FIRMWARE_IMAGE_DESCRIPTOR provided by FMP.GetImageInfo().

The resource entry field FwClass must be set  to “ESRT_FW_TYPE_SYSTEMFIRMWARE”
The fields LowestSupportedVersion, FwVersion are provided by
- the Secure World FWU when Secure world is responsible for the update
- Non-secure firmware if Non-Secure world is responsible for the update

LastAttemptStatus is expected to be information kept by the UEFI implementation. This can partially
rely on information provided by early platform bootstages [FWU]_.


The acceptance status of each FW image is provisionally displayed in the LastAttemptedStatus field [#provisional]_ in the ESRT image entry.
A value of 0x3fff implies that the image has not been accepted. The OS must
explicitly accept the image by installing an acceptance capsule as described in `OS directed FW image acceptance`_.

.. [#provisional] Presenting the image acceptance status in the LastAttemptedStatus field is a provisional arrangement. A more permanet solution is under discussion.

A UEFI implementation can opt to implicitly accept any FW image. It is
strongly recommended that the FW image acceptance is treated as an OS responsibility.

.. _revert-sec:

Rollback Counters
-----------------

Devices will have their own platform specific way of implementing rollback counters
E.g some devices might rely on fuses or similar hardware. Other platforms
might choose to keep the rollback counter as part of an authenticated image
which is protected by the platfrom Chain of Trust.

Since this specification relies on the existing [UEFI]_ specification, the
rollback counters must be implemeted using  the `Firmware Management Protocol
<https://uefi.org/specs/UEFI/2.9_A/23_Firmware_Update_and_Reporting.html#firmware-management-protocol>`_
and specifically the **LowestSupportedImageVersion** of the image descriptor.

Implementing the mapping between the internal rollback counter values and the
**LowestSupportedImageVersion** is outside the scope of this document.

OS requested FW revert
----------------------

The UEFI implementation informs the OS of the current FW images via the ESRT.

The OS can request a FW downgrade. The FW downgrade request is a hint to UEFI that the FW bank should be moved to a previously working bank.

If the OS wants to revert the FW images to a previously working bank, it can do so by installing the following signed capsule:
**Note**: Reverting a firmware while in trial state is prohibited

- CapsuleGuid = acd58b4b-c0e8-475f-99b5-6b3f7e07aaf0
- HeaderSize = sizeof(EFI_CAPSULE_HEADER)
- Flags = 0
- CapsuleImageSize = sizeof(EFI_CAPSULE_HEADER)

**Note**: the image acceptance capsule must be authenticated. Details TBD.

When UEFI receives the capsule above, UEFI will change the FWU metadata active_index to a previously working bank index by either:

#. Calling the FWU primitive fwu_set_active [FWU]_ if the flash store is owned the Secure World.
#. Set the active_index field in the FWU Metadata [FWU]_ if the flash store is owned by the Normal World.
#. Restore EFI variables to their previous state, if changed during the firmware upgrade.

OS directed FW image acceptance
-------------------------------

OS declaration of future capsule acceptance responsibility
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The OS can inform the UEFI implementation of all the FW images that the OS intends to explicitly accept.

The OS can set the EFI_CAPSULE_HEADER.Flags[15] to 1 indicating that it will explicitly accept all the FW images contained in this capsule.
The OS must be able to accept all images that it registers to accept. Any other images should be implicitly accepted by the UEFI implementation.

FW image acceptance
^^^^^^^^^^^^^^^^^^^

The OS must accept each image, that has an acceptance pending, by using a capsule composed of an EFI_CAPSULE_HEADER concatenated with the image type UUID:
**Note**: Performing a capsule update while in trial state is prohibited.

- CapsuleGuid 0c996046-bcc0-4d04-85ec-e1fcedf1c6f8
- HeaderSize = sizeof(EFI_CAPSULE_HEADER)
- Flags = 0
- CapsuleImageSize = sizeof(EFI_CAPSULE_HEADER) + sizeof(UUID)


**Note**: the image acceptance capsule must be authenticated. Details TBD.

Update permission verification
------------------------------

The FW management guidelines in [NIST_800_193]_ specify that the system should check:

#. FW image authenticity.
#. FW update procedure authorization.

The FW image authenticity should be implemented by authenticating the different FW images.
The FW update authorization should be implemented by verifying that the capsule or its components were assembled
by the platform owner.

FW update authorization
^^^^^^^^^^^^^^^^^^^^^^^

The FW update authorization [NIST_800_193]_ can be checked by the OS, using OS specific methods, before calling
UpdateCapsule. Alternatively, the FW update authorization can rely on the FW image authenticity check.
If all FW images in the capsule are authentic then the user is deemed authorized to progress with the FW update procedure.

FW image authentication
^^^^^^^^^^^^^^^^^^^^^^^

Each FW image should be signed by the FW vendor. The mechanism for FW image vendor public
key to be provisioned is outside the scope of this document.

The FW vendor signature should be placed before the FW image as is described in the UEFI FMP definition
(§ 23.1 [UEFI]_).

The FW images should be authenticated before being written to the FW store or before being
allowed to execute on the platform.

Maximum Trial platform boots
----------------------------

The UEFI implementation must keep a count of the consecutive platform boots in
the Trial state [FWU]_.  If the number of consecutive platform boot in the
Trial state exceeds a platform defined value of *max_trial_boots* then the UEFI
implementation must revert the FW to the previous working bank [FWU]_.

**Note**: Similar functionality must be implemented in the first stage boot loader.
This is platform specific, but the first stage bootloader must be able to count
the number of reboots. If the number exceeds *max_trial_boots* then we must
revert to a previous working version of the firmware.

