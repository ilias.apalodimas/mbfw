.. Dependable Boot Source Document
   Copyright Linaro Limited, 2020-2021
   SPDX-License-Identifier: CC-BY-SA-4.0

##############################
Dependable Boot  Specification
##############################

Copyright © Linaro Limited and Contributors.

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
International License. To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

.. image:: images/cc-by-sa-4.0-88x31.*
   :target: http://creativecommons.org/licenses/by-sa/4.0/
   :alt: Creative Commons License
   :align: right


.. toctree::
   :numbered:

   chapter1-about
   chapter2-uefi
   chapter3-fwupdate
   chapter4-failsafe
   conventions
   references
